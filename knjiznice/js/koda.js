const baseUrl = 'https://rest.ehrscape.com/rest/v1';
const queryUrl = baseUrl + '/query';

const username = "ois.seminar";
const password = "ois4fri";

var currentHeight = 0;
var currentWeight = 0;
var currentSystolic = 0;
var currentDiastolic = 0;
var currentSmoker = "";
var currentActivity = "";
var currentSugar = 0;
var currentHolesterol = 0;

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    const response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
        "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function createPatient(name, surname, dob, smoker, activity) {
    sessionId = getSessionId();

    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    });

    $.ajax({
        url: baseUrl + '/ehr',
        type: 'POST',
        success: function (data) {
            ehrId = data.ehrId;
            const partyData = {
                firstNames: name,
                lastNames: surname,
                dateOfBirth: dob,
                partyAdditionalInfo: [{
                    key: "ehrId",
                    value: ehrId
                }, {
                    key: "smoker",
                    value: smoker
                }, {
                    key: "activity",
                    value: activity
                }]
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function(party) {
                    if (party.action === 'CREATE') {
                        $("#createUserNotification").html(
                            "<div class='alert alert-success' role='alert'>" +
                            "Pacient je uspesšno kreiran. EHR ID: " + ehrId +
                            "</div>"
                        );
                    }
                },
                error: function(err) {
                    $("#kreiranjeObvestilo").html(
                        "<div class='alert alert-danger' role='alert'>" +
                        "Napaka: pacienta ni bilo mogoče kreirati" +
                        "</div>"
                    );
                }
            });
        }
    });
}

function addObservation(id, date, systolic, diastolic, weight, height, sugar, holesterol) {
    sessionId = getSessionId();

    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    });

    const data = {
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": date,
        "vital_signs/body_weight/any_event/body_weight": weight,
        "vital_signs/height_length/any_event/body_height_length": height,
        "vital_signs/blood_pressure/any_event/systolic": systolic,
        "vital_signs/blood_pressure/any_event/diastolic": diastolic,
        "vital_signs/body_temperature/any_event/temperature|magnitude": sugar,
        "vital_signs/pulse/any_event/rate|magnitude": holesterol,
        "vital_signs/body_temperature/any_event/temperature|unit": "°C"
    };

    const parameters = {
        ehrId: id,
        templateId: 'Vital Signs',
        format: 'FLAT',
    };

    $.ajax({
        url: baseUrl + "/composition?" + $.param(parameters),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function() {
            $("#addObservationNotification").html(
                "<div class='alert alert-success' role='alert'>" +
                "Meritve so bile uspešno poslane."+
                "</div>"
            );
        },
        error: function() {
            $("#addObservationNotification").html(
                "<div class='alert alert-danger' role='alert'>" +
                "Pri shranjevanju meritev je prišlo do napake."+
                "</div>"
            );
        }
    });
}

function getObservation(id) {
    sessionId = getSessionId();
    var points = 0;
    $.ajax({
        url: baseUrl + "/demographics/ehr/" + id + "/party",
        type: 'GET',
        headers: {
            "Ehr-Session": sessionId
        },

        success: function (data) {
            console.log(data.party);
            for (let additional of data.party.partyAdditionalInfo) {
                if (additional["key"] === "smoker") {
                    currentSmoker = additional["value"];
                }
                if (additional["key"] === "activity") {
                    currentActivity = additional["value"];
                }
            }

            $("#nameInfo").html(data.party.firstNames + " " + data.party.lastNames);
            $("#dobInfo").html(data.party.dateOfBirth);
            if (currentSmoker === "on") {
                points++;
                $("#smokerInfo").html("DA").css("color", "red");
            } else {
                $("#smokerInfo").html("NE").css("color", "green");
            }

            if (currentActivity === "manj kot 1 uro na teden") {
                points += 2;
                $("#activityInfo").html(currentActivity).css("color", "red");
            } else if (currentActivity === "do 2 uri na teden") {
                points++;
                $("#activityInfo").html(currentActivity).css("color", "orange");
            } else {
                $("#activityInfo").html(currentActivity).css("color", "green");
            }

            $.ajax({
                url: baseUrl + "/view/" + id + "/weight",
                type: 'GET',
                headers: {
                    "Ehr-Session": sessionId
                },
                success: function(res) {
                    currentWeight = res[0].weight;
                    $("#dateObservationInfo").html(res[0].time);
                    $("#weightInfo").html(currentWeight);
                }
            });

            $.ajax({
                url: baseUrl + "/view/" + id + "/height",
                type: 'GET',
                headers: {
                    "Ehr-Session": sessionId
                },
                success: function(res) {
                    currentHeight = res[0].height;
                    $("#heightInfo").html(currentHeight);
                }
            });

            $.ajax({
                url: baseUrl + "/view/" + id + "/body_temperature",
                type: 'GET',
                headers: {
                    "Ehr-Session": sessionId
                },
                success: function(res) {
                    currentSugar = res[0].temperature;
                    if (currentSugar > 3.5 && currentSugar < 6.2) {
                        $("#sugarInfo").html(currentSugar).css("color", "green");
                    } else {
                        points++;
                        $("#sugarInfo").html(currentSugar).css("color", "red");
                    }
                }
            });

            $.ajax({
                url: baseUrl + "/view/" + id + "/pulse",
                type: 'GET',
                headers: {
                    "Ehr-Session": sessionId
                },
                success: function(res) {
                    console.log(res[0]);
                    currentHolesterol = res[0].pulse;
                    if (currentHolesterol < 3) {
                        $("#holesterolInfo").html(currentHolesterol).css("color", "green");
                    } else {
                        points++;
                        $("#holesterolInfo").html(currentHolesterol).css("color", "red");
                    }
                }
            });

            $.ajax({
                url: baseUrl + "/view/" + id + "/" + "blood_pressure",
                type: 'GET',
                headers: {
                    "Ehr-Session": sessionId
                },
                success: function(res) {
                    currentSystolic = res[0].systolic;
                    currentDiastolic = res[0].diastolic;
                    if (res[0].systolic < 140 && res[0].systolic > 130) {
                        $("#systolicInfo").html(res[0].systolic).css("color", "green");
                    } else {
                        points++;
                        $("#systolicInfo").html(res[0].systolic).css("color", "red");
                    }
                    if (res[0].diastolic < 86 && res[0].diastolic > 79) {
                        $("#diastolicInfo").html(res[0].diastolic).css("color", "green");
                    } else {
                        points++;
                        $("#diastolicInfo").html(res[0].diastolic).css("color", "red");
                    }
                    if (points < 3) {
                        $("#strokeInfo").html("<div class='alert alert-success' role='alert'>" +
                            "Možnost možganske kapi zelo majhna." +
                            "</div>");
                    } else if (points < 5) {
                        $("#strokeInfo").html("<div class='alert alert-warning' role='alert'>" +
                            "Povišano tveganje za možgansko kap." +
                            "</div>");
                    } else {
                        $("#strokeInfo").html("<div class='alert alert-danger' role='alert'>" +
                            "Zelo visoko tveganje možganske kapi!" +
                            "</div>");
                    }
                }
            });

        }
    });
}

$(document).ready(function() {
    $("#createUser").click(function () {
        const name = $("#name").val();
        const surname = $("#surname").val();
        const dob = $("#dob").val();
        const smoker = $("#smoker").val();
        const activity = $("#activity").val();

        if (!name || !surname || !dob) {
            $("#createUserNotification").html(
                "<div class='alert alert-danger' role='alert'>" +
                "Podatki, ki ste jih vnesli niso popolni"+
                "</div>"
            )
        } else {
            createPatient(name, surname, dob, smoker, activity);
        }
    });

    $("#addObservation").click(function() {
        const id = $("#ehr").val();
        const date = $("#date").val();
        const weight = $("#weight").val();
        const height = $("#height").val();
        const systolic = $("#systolic").val();
        const diastolic = $("#diastolic").val();
        const sugar = $("#sugar").val();
        const holesterol = $("#holesterol").val();

        if (!id || !date || !systolic || !diastolic || !weight || !height || !sugar || !holesterol) {
            $("#addObservationNotification").html(
                "<div class='alert alert-danger' role='alert'>" +
                "Vsa polja so obvezna"+
                "</div>"
            )
        } else {
            addObservation(id, date, systolic, diastolic, weight, height, sugar, holesterol);
        }
    });

    $("#getData").click(function () {
        const id = $("#ehr-analysis").val();

        if (id) {
            getObservation(id);
        }
    });
});

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}

// GAUGE GRAPH

window.feed = function(callback) {
    var tick = {};
    height = currentHeight/100;
    console.log(height, currentWeight);
    tick.plot0 = Math.ceil((currentWeight/height)/height);
    callback(JSON.stringify(tick));
};

var myConfig = {
    type: "gauge",
    globals: {
        fontSize: 12
    },
    plotarea: {
        marginTop: 80
    },
    plot: {
        size: '100%',
        valueBox: {
            placement: 'center',
            text: '%v', //default
            fontSize: 12,
            rules: [{
                rule: '%v <= 14.9',
                text: '%v<br>Huda nedohranjenost'
            }, {
                rule: '%v > 14.9 && %v <= 15.9',
                text: '%v<br>Zmerna nedohranjenost'
            }, {
                rule: '%v > 15.9 && %v <= 18.4',
                text: '%v<br>Blaga nedohranjenost'
            }, {
                rule: '%v > 18.4 && %v <= 24.9',
                text: '%v<br>Normalna telesna teža'
            }, {
                rule: '%v > 24.9 && %v <= 29.9',
                text: '%v<br>Povišana telesna teža'
            }, {
                rule: '%v > 29.9 && %v <= 34.9',
                text: '%v<br>Debelost razred I'
            }, {
                rule: '%v > 34.9 && %v <= 39.9',
                text: '%v<br>Debelost razred II'
            }, {
                rule: '%v > 39.9',
                text: '%v<br>Debelost razred III'
            }]
        }
    },
    tooltip: {
        borderRadius: 5
    },
    scaleR: {
        aperture: 180,
        minValue: 8,
        maxValue: 42,
        step: 0.1,
        center: {
            visible: false
        },
        tick: {
            visible: false
        },
        item: {
            offsetR: 0,
            rules: [{
                rule: '%i == 9',
                offsetX: 15
            }]
        },
        ring: {
            size: 20,
            rules: [{
                rule: '%v <= 14.9',
                backgroundColor: '#ed5f55'
            }, {
                rule: '%v > 14.9 && %v <= 15.9',
                backgroundColor: '#f87f52'
            }, {
                rule: '%v > 15.9 && %v <= 18.4',
                backgroundColor: '#fcce54'
            }, {
                rule: '%v > 18.4 && %v <= 24.9',
                backgroundColor: '#c2d468'
            }, {
                rule: '%v > 24.9 && %v <= 29.9',
                backgroundColor: '#fcce54'
            }, {
                rule: '%v > 29.9 && %v <= 34.9',
                backgroundColor: '#f87f52'
            }, {
                rule: '%v > 34.9 && %v <= 39.9',
                backgroundColor: '#ed5f55'
            }, {
                rule: '%v > 39.9',
                backgroundColor: '#da4d44'
            }]
        }
    },
    refresh: {
        type: "feed",
        transport: "js",
        url: "feed()",
        interval: 1500,
        resetTimeout: 1000
    },
    series: [{
        values: [20], // starting value
        backgroundColor: 'black',
        indicator: [2, 2, 2, 2, 0.82],
        animation: {
            effect: 2,
            method: 1,
            sequence: 4,
            speed: 900
        },
    }]
};

// STATISTIC DATA GRAPH

var parseTime = d3.timeParse("%Y");

var svg = d3.select("svg");

var margin = {top: 30, right: 50, bottom: 30, left: 30},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom,
    labelPadding = 3;

var g = svg.append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

d3.requestTsv("data.tsv", function(d) {
    d.date = parseTime(d.date);
    for (var k in d) if (k !== "date") d[k] = +d[k];
    return d;
}, function(error, data) {
    if (error) throw error;

    var series = data.columns.slice(1).map(function(key) {
        return data.map(function(d) {
            return {
                key: key,
                date: d.date,
                value: d[key]
            };
        });
    });

    var x = d3.scaleTime()
        .domain([data[0].date, data[data.length - 1].date])
        .range([0, width]);

    var y = d3.scaleLinear()
        .domain([0, d3.max(series, function(s) { return d3.max(s, function(d) { return d.value; }); })])
        .range([height, 0]);

    var z = d3.scaleCategory10();

    g.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

    var serie = g.selectAll(".serie")
        .data(series)
        .enter().append("g")
        .attr("class", "serie");

    serie.append("path")
        .attr("class", "line")
        .style("stroke", function(d) { return z(d[0].key); })
        .attr("d", d3.line()
            .x(function(d) { return x(d.date); })
            .y(function(d) { return y(d.value); }));

    var label = serie.selectAll(".label")
        .data(function(d) { return d; })
        .enter().append("g")
        .attr("class", "label")
        .attr("transform", function(d, i) { return "translate(" + x(d.date) + "," + y(d.value) + ")"; });

    label.append("text")
        .attr("dy", ".35em")
        .text(function(d) { return d.value; })
        .filter(function(d, i) { return i === data.length - 1; })
        .append("tspan")
        .attr("class", "label-key")
        .text(function(d) { return " " + d.key; });

    label.append("rect", "text")
        .datum(function() { return this.nextSibling.getBBox(); })
        .attr("x", function(d) { return d.x - labelPadding; })
        .attr("y", function(d) { return d.y - labelPadding; })
        .attr("width", function(d) { return d.width + 2 * labelPadding; })
        .attr("height", function(d) { return d.height + 2 * labelPadding; });
});
